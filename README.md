# IGB Quickload tools #

This repository contains (mostly) python code for creating and managing IGB
Quickload sites.

Most scripts deal with writing annots.xml files for genome version directories
in IGB quickload sites:

* [Main IGB Quickload site](http://igbquickload.org/quickload) (hosts sequence files)
* [About annots.xml](https://wiki.transvar.org/display/igbman/About+annots.xml)

This repository also contains documentation describing how we maintain and set up
subversion for managing the main IGB Quickload site.

# What is IGB Quickload? #

In a nutshell, IGB Quickload is a REST protocol for sharing data files via the Web. 
The IGB project uses IGB Quickload to enable IGB users to access data files hosted 
on many different sites and view them within the same interface. 

IGB's ability to show data sets from different groups in the same integrated view
is why we named IGB the "Integrated Genome Browser" back in the early 2000s, when we
first released it. Other genome browser systems have re-implemented this idea, notably the
UCSC Genome Browser's Track Hubs concept. 

**Note**: One benefit of using IGBQuickload is that you can host the data files anywhere, 
provided they are accessible via http or https. Often a IGBQuickload site simply hosts the 
meta data files describing the data collection, but the (typically large) data files reside 
somewhere else, e.g., an S3 bucket in Amazon.

# Example Quickload site #

The [BioAnalytic Resource Quickload site](http://lorainelab-quickload.scidas.org/bar/) provides access to RNA-Seq data sets from the model organism Arabidopsis thalana. 

Note that this site contains only the following files:

```
bar
├── A_thaliana_Jun_2009
│   ├── annots.xml
│   └── genome.txt
├── contents.txt
└── favicon.ico
```

The `annots.xml` file contains links to data files that reside in an Amazon S3 bucket, accessible via http or https. (We use http in this case.)

## Setting up

To use scripts and code in this repository

* Clone the repo
* Add the cloned repo to your PATH and PYTHONPATH

