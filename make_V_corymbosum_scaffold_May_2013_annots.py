#!/usr/bin/env python

from Quickload import *
from AnnotsXmlForSmMmRNASeq import *
import QuickloadUtils as utils

import os,sys

genome='V_corymbosum_scaffold_May_2013'
track_info_url='V_corymbosum_scaffold_May_2013'
bb_url='V_corymbosum_scaffold_May_2013/bb_se_TH2.0.6_6K_processed'
dev_course_folder='RNA-Seq Berry Development'
ripe="0000ff"
pink="FF55FF"
green="006600"
cup="339966"
pad="00FF00"
w="FFFFFF"

bb_se=[['2-41_ripe','ripe 2-41',ripe,w,bb_url],
       ['2-42_ripe','ripe 2-42',ripe,w,bb_url],
       ['3-33_ripe','ripe 3-33',ripe,w,bb_url],
       
       ['2-41_pink','pink 2-41',pink,w,bb_url],
       ['2-42_pink','pink 2-42',pink,w,bb_url],
       ['3-33_pink','pink 3-33',pink,w,bb_url],
       
       ['2-41_green','green 2-41',green,w,bb_url],
       ['2-42_green','green 2-42',green,w,bb_url],
       ['3-33_green','green 3-33',green,w,bb_url],
       
       ['2-41_cup','cup 2-41',cup,w,bb_url],
       ['2-42_cup','cup 2-42',cup,w,bb_url],
       ['3-33_cup','cup 3-33',cup,w,bb_url],
       
       ['2-41_pad','pad 2-41',pad,w,bb_url],
       ['3-33_pad_a','pad 3-33a',pad,w,bb_url],
       ['3-33_pad_b','pad 3-33b',pad,w,bb_url]]

def get454():
    quickload_files=[]
    # DHMRI green
    folder = "454 Alignments/2009 O'Neal berries/"
    dhmri_454_green='GI3MI6V02'
    descr=description="Green berries harvested May 2009 from 2-41, GMAP alignments of trimmed sequences"
    fn=dhmri_454_green+'.fastq_trimmed_sorted.bam'
    title=folder + "Green O'Neal"
    quickload_file=BamFile(path=fn,track_name=title,tool_tip=description,foreground_color=green)
    quickload_files.append(quickload_file)

    # DHMRI ripe
    dhmri_454_ripe='GI3MI6V01'
    descr=description="Ripe berries harvested June 2009 from 2-40,41,42, GMAP alignments of trimmed sequences"
    fn=dhmri_454_ripe+'.fastq_trimmed_sorted.bam'
    title=folder + "Ripe O'Neal"
    quickload_file=BamFile(path=fn,track_name=title,tool_tip=description,foreground_color=ripe)
    quickload_files.append(quickload_file)


    # NCSU green
    folder = "454 Alignments/2010 O'Neal berries/"
    ncsu_454_green='onunr'
    descr=description="Green O'Neal berries harvested 2010, GMAP alignments of trimmed sequences"
    fn=ncsu_454_green+'.fastq_trimmed_sorted.bam'
    title=folder + "Green O'Neal"
    quickload_file=BamFile(path=fn,track_name=title,tool_tip=description,foreground_color=green)
    quickload_files.append(quickload_file)

    # NCSU ripe
    ncsu_454_ripe='onrip'
    descr=description="Ripe O'Neal berries harvested 2010, GMAP alignments of trimmed sequences"
    fn=ncsu_454_ripe+'.fastq_trimmed_sorted.bam'
    title=folder + "Ripe O'Neal"
    quickload_file=BamFile(path=fn,track_name=title,tool_tip=description,foreground_color=ripe)
    quickload_files.append(quickload_file)


    # Rowland lab
    study='SRA047024'
    # this was helpful
    # http://sra.dnanexus.com/runs/SRR353291
    path='V_corymbosum_scaffold_May_2013/01_bams/'
    lst=['SRR353282',
         'SRR353283',
         'SRR353285',
         'SRR353286',
         'SRR353287',
         'SRR353288',
         'SRR353289',
         'SRR353290',
         'SRR353291']
    d = {'SRR353282':'MID1',
         'SRR353283':'MID2',
         'SRR353285':'MID3',
         'SRR353286':'MID4',
         'SRR353287':'MID5',
         'SRR353288':'MID6',
         'SRR353289':'MID7',
         'SRR353290':'MID8',
         'SRR353291':'MID10'}
    folder = '454 Alignments/Rowland Lab/'
    descr='Sequences from Short Read Archive study %s from USDA project led by Jeannie Rowland'%study
    #u=path
    for name in lst:
        sample=d[name]
        title=folder+sample + ' ' + name
        fn='01_bams/'+name+'.fastq.fasta_sorted.bam'
        quickload_file=BamFile(path=fn,track_name=title,tool_tip=description,
                               foreground_color='0033CC')
        quickload_files.append(quickload_file)
    for quickload_file in quickload_files:
        quickload_file.setTrackInfoUrl(track_info_url)
    return quickload_files

def getBbGeneModels():
    quickload_files=[]

    # reference gene models, these should autoload
    title = "Blueberry genes"
    descr="Reference gene models from Vikas Gupta's annotation pipeline."
    fn="V_corymbosum_scaffold_May_2013_wDescrPwy.bed.gz"
    foreground_color="000000"
    quickload_file = AnnotationFile(path=fn,tool_tip=descr,
                                    track_name=title,foreground_color=foreground_color,
                                    load_all=True)
    quickload_files.append(quickload_file)

    # other gene models predicted by various programs
    title="Gene Predictions/CuffLinks"
    descr="CuffLinks gene models from RNA-Seq Berry Development"
    foreground_color="3300FF"
    fn="Cufflinks_6K.bed.gz"
    quickload_file = AnnotationFile(path=fn,tool_tip=descr,
                                    track_name=title,foreground_color=foreground_color)
    quickload_files.append(quickload_file)

    foreground_color='CC0033'
    fn="11_CombineGFF3/genemark.bed.gz"
    title="Gene Predictions/Genemark"
    descr="Genemark gene models"
    quickload_file = AnnotationFile(path=fn,tool_tip=descr,
                                    track_name=title,foreground_color=foreground_color)
    quickload_files.append(quickload_file)
    
    fn="11_CombineGFF3/glimmer.bed.gz"
    title="Gene Predictions/Glimmer"
    descr="Glimmer gene models"
    foreground_color='9900FF'
    quickload_file = AnnotationFile(path=fn,tool_tip=descr,
                                    track_name=title,foreground_color=foreground_color)
    quickload_files.append(quickload_file)

    foreground_color='009900'
    fn="11_CombineGFF3/augustus.bed.gz"
    title="Gene Predictions/Augustus"
    descr="Augustus gene models"
    quickload_file = AnnotationFile(path=fn,tool_tip=descr,
                                    track_name=title,foreground_color=foreground_color)
    quickload_files.append(quickload_file)
    for quickload_file in quickload_files:
        quickload_file.setTrackInfoUrl(track_info_url)
    return quickload_files

def getESTs():
    quickload_files=[]
    foreground_color='CC0000'
    fn="EST.bed.gz"
    title="EST"
    descr="GMAP alignments of blueberry ESTs and mRNAs from dbEST and Genbank"
    quickload_file = AnnotationFile(path=fn,tool_tip=descr,
                                    track_name=title,foreground_color=foreground_color,
                                    show2tracks=False,
                                    label_field="id")
    quickload_files.append(quickload_file)
    for quickload_file in quickload_files:
        quickload_file.setTrackInfoUrl(track_info_url)
    return quickload_files

def makeQuickloadFiles():
    quickload_files = []
    for quickload_file in getBbGeneModels():
        quickload_files.append(quickload_file)
    for quickload_file in makeQuickloadFilesForRNASeq(lsts=bb_se,
                                                      folder=dev_course_folder,
                                                      deploy_dir=bb_url.split('/')[1],
                                                      all=True):
        quickload_files.append(quickload_file)
    for quickload_file in getESTs():
        quickload_files.append(quickload_file)
    for quickload_file in get454():
        quickload_files.append(quickload_file)
    return quickload_files

if __name__ == '__main__':
    about="Make annots.xml text." 
    utils.checkForHelp(about)
    quickload_files=makeQuickloadFiles()
    main(quickload_files)
