#!/usr/bin/env python

"""Make annots.xml from a text file."""

import Quickload as quickload
import QuickloadUtils as utils

import os,sys

if __name__ == '__main__':
    about="Create annots.xml text using a tab-delimited, text data streamed from stdin.\nFirst row (line) of data contains annots.xml file attribute names.\nSubsequent rows contain attribute values. Each row is one Quickload file.\n\nex)\n\n cat file.txt | %prog > annots.xml\n cat file.txt | %prog annots.xml"
    utils.checkForHelp(about) 
    quickload_files=utils.makeQuickloadFileObjectsFromTableData()
    quickload.main(quickload_files=quickload_files)
