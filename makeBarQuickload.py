#!/usr/bin/env python

"""Make annots.xml for BAR Quickload site. Make Quickload directories if they do not already exist. Copy content from contents.txt and genome.txt for genome version from authoritative, main Quickload site. Deploy to host."""

from Quickload import *
from AnnotsXmlForRNASeq import *
import QuickloadUtils as utils
import xml.etree.ElementTree as ET
import os,re,urllib,optparse

urls=["https://raw.githubusercontent.com/ASully/eFP-Seq-Browser/master/cgi-bin/data/bamdata_amazon_links.xml",
        "https://raw.githubusercontent.com/ASully/eFP-Seq-Browser/master/cgi-bin/data/bamdata_Developmental_transcriptome.xml"]

def ensure_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

def readBarUrl(url=None):
    if not url:
        url = urls[0]
    f = urllib.urlopen(url)
    tree = ET.parse(f)
    root_node = tree.getroot()
    nodes = []
    for item in root_node.findall('./file'):
        nodes.append(item)
    dataset_title = root_node.attrib["xmltitle"]
    return (dataset_title,nodes)

def makeBarQuickload(options=None):
    root_dir = None
    if options:
        root_dir = options.root_dir
    if not root_dir:
        root_dir="bar"
    gversion="A_thaliana_Jun_2009"
    authority_quickload = "http://igbquickload.org/quickload/"
    ensure_dir(root_dir)
    ensure_dir(root_dir+"/"+gversion)
    favicon_url="https://bitbucket.org/lorainelab/igbquickload/raw/master/assets/bar/favicon.ico"
    urllib.urlretrieve(favicon_url,root_dir+"/favicon.ico")
    fh = urllib.urlopen(authority_quickload+"contents.txt")
    lines = fh.read().split("\n")
    fh.close()
    for line in lines:
        if re.match(gversion,line):
            fh = open(root_dir+"/contents.txt","w")
            fh.write(line+"\n")
            break
    fh = urllib.urlopen(authority_quickload+gversion+"/genome.txt")
    data = fh.read()
    fh.close()
    fh = open(root_dir+"/"+gversion+"/genome.txt",'w')
    fh.write(data)
    fh.close()
    annots_file = root_dir + "/" + gversion + "/annots.xml"
    quickload_files=makeQuickloadFiles()
    txt = makeAnnotsXml(quickload_files=quickload_files)
    fh=open(annots_file,'w')
    fh.write(txt)
    fh.close()
    if options:
        server_dir = options.server_dir
        if server_dir:
            cmd = "scp -r %s %s/."%(root_dir,server_dir)
            os.system(cmd)
    
            

def makeQuickloadFiles():
    quickload_files = []
    for url in urls:
        (dataset_name,nodes)=readBarUrl(url=url)
        for node in nodes:
            d = node.attrib
            description = d["description"]
            # no need to indicate species
            description = re.sub(r'^Arabidopsis thaliana ','',description)
            title = dataset_name + " / " + description + " / " + d["record_number"] + " " + description
            path=d["name"]
            # no encryption
            path=re.sub('https','http',path)
            url=d["url"]
            tool_tip=d["total_reads_mapped"]+" reads. "+ d["info"]
            foreground_color=d["foreground"]
            if (foreground_color.startswith("0x")):
                    # some of them do (bug?)
                    foreground_color=foreground_color[2:]
            quickload_file = BamFile(path=path,track_name=title,track_info_url=url,
                                         tool_tip=tool_tip,foreground_color=foreground_color,
                                         show2tracks=False,direction_indicator=None)
            quickload_files.append(quickload_file)
    return quickload_files

if __name__ == '__main__':
    usage = "usage: %prog [options]"
    parser = optparse.OptionParser(usage=usage)
    parser.add_option("-r","--root_dir",
                           dest="root_dir",
                           default="bar",
                           help='name of Quickload root directory to create, "bar" if not provided')
    parser.add_option("-s","--server_dir",
                          dest="server_dir",
                          help="Host and path on host where root directory will be copied via scp.\n\nex) hostname:/var/www/bioviz/htdocs/quickloads")
    (options,args)=parser.parse_args()
    makeBarQuickload(options)
