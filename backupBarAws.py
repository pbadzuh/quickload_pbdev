#!/usr/bin/env python

"""Mirror BioAnalytic Resource (BAR) AWS S3 on local storage for safe-keeping and testing"""

import requests
from contextlib import closing
import xml.etree.ElementTree as ET
import os,re,urllib,optparse,sys,os.path

# Alex Sullivan's github
bamdata_urls=["https://raw.githubusercontent.com/ASully/eFP-Seq-Browser/master/cgi-bin/data/bamdata_amazon_links.xml",
        "https://raw.githubusercontent.com/ASully/eFP-Seq-Browser/master/cgi-bin/data/bamdata_Developmental_transcriptome.xml"]

"Make sure that the given directory exists on the local file system."
def ensure_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

"Check that the given URLs exist and can be retrieved" 
def checkUrls(urls=None,stop_at=None):
    ok_urls = {}
    bad_urls = []
    if not urls:
        urls = getBamUrls()
    num_urls=0
    for url in urls:
        if stop_at and num_urls == stop_at:
            break
        result = check_url(url)
        if result > 0:
            ok_urls[url]=result
        else:
            bad_urls.append(url)
        num_urls = num_urls + 1
    sys.stderr.write("Tested: %i\n"%num_urls)
    sys.stderr.write("Bad urls: %i\n"%len(bad_urls))
    sys.stderr.write("OK urls: %i\n"%len(ok_urls))
    return {"ok":ok_urls,"bad":bad_urls}

"Check that the given resource exists and can be retrieved" 
def check_url(url=None):
    with closing(requests.get(url,stream=True)) as r:
        if r.status_code==requests.codes.ok:
            return int(r.headers['Content-Length'])
        else:
            return 0

"Find out how much data. As of 9/1/18, there was around 283 Gb"
def howBig(check_url_results=None):
    if not check_url_results:
        check_url_results = checkUrls()
    sizes = check_url_results["ok"].values()
    return(sum(sizes))

"Retrieve and parse the given XML listing of remote BAM files curated by BAR"        
def readBarUrl(url=None):
    if not url:
        url = bamdata_urls[0]
    f = urllib.urlopen(url)
    tree = ET.parse(f)
    root_node = tree.getroot()
    nodes = []
    for item in root_node.findall('./file'):
        nodes.append(item)
    dataset_title = root_node.attrib["xmltitle"]
    return (dataset_title,nodes)

"Retrieve a list of remote file URLs"
def getBamUrls():
    to_return = []
    for url in bamdata_urls:
        (dataset_name,nodes)=readBarUrl(url=url)
        for node in nodes:
            d = node.attrib
            path=d["name"]
            # no encryption
            path=re.sub('https','http',path)
            to_return.append(path)
    return to_return

"Check that the given remote BAM files have a .bai index file."
def checkBaiUrls(urls=None,stop_at=None):
    if not urls:
        urls = getBamUrls()
    new_urls = []
    for url in urls:
        new_urls.append(url+".bai")
    return checkUrls(urls=new_urls,stop_at=stop_at)
        
def backupFileLocally(url=None,path_splitter="araport",remote_size=None):
    if not remote_size:
        remote_size=check_url(url=url)
    local_path=url.split(path_splitter)[1]
    toks = local_path.split("/")
    toks = filter(lambda x:len(x)>0,toks)
    fname = toks[-1]
    local_dirs = os.sep.join(toks[:-1])
    local_path = os.sep.join([local_dirs,fname])
    if not os.path.isfile(local_path):
        ensure_dir(local_dirs)
        sys.stderr.write("need to get: %s\n"%local_path)
        urllib.urlretrieve(url,local_path)
    return os.path.isfile(local_path) and os.stat(local_path).st_size==remote_size

def doIt(stop_at=None):
    # get URLs for BAM files
    bam_urls = getBamUrls()
    bai_total=0
    bam_total=0
    checked=0
    for bam_url in bam_urls:
        if stop_at and checked==stop_at:
            break
        remote_size=check_url(url=bam_url)
        if remote_size == 0:
            raise ValueError("Problem accessing %s"%bam_url)
        result1 = backupFileLocally(url=bam_url,remote_size=remote_size)
        checked=checked+1
        if result1:
            bam_total=bam_total+1
        bai_url = bam_url + ".bai"
        remote_size=check_url(url=bai_url)
        if remote_size == 0:
            raise ValueError("Problem accessing %s"%bai_url)
        result2 = backupFileLocally(url=bai_url,remote_size=remote_size)
        if result2:
            bai_total=bai_total+1
    sys.stderr.write("%i BAM files checked\n"%checked)
    sys.stderr.write("%i BAM and %i BAI files found or newly backed up\n"%(bam_total,bai_total))
    
if __name__ == '__main__':
    usage = "usage: %prog [options]"
    parser = optparse.OptionParser(usage=usage)
    (options,args)=parser.parse_args()
    doIt()
