#!/usr/bin/env python

from Quickload import *
import QuickloadUtils as utils

genome="A_thaliana_Jun_2009"

# contains files on Quickload site
deploy_dir='LFYChIPSeq/'

# folder names for Available Data section of Data Access tab
igb_folder='ChIP-Seq/LFY SRP003928/'

# URL user's browser opens when they click the "i" in Available Data section
# URL is relative to the Quickload root
track_info_url=genome+'/'+deploy_dir

def makeQuickloadFiles():
    quickload_files = []
    fname=deploy_dir+"LFY_filtered_summit_extended.bed.gz"
    foreground_color="4D4DFF"
    description="high confidence binding regions"
    track_name=igb_folder+"LFY filtered summits"
    quickload_file=AnnotationFile(path=fname,
                           foreground_color=foreground_color,
                           show2tracks=False,
                           direction_indicator="none",
                           tool_tip=description,
                           track_name=track_name,
                           track_info_url=track_info_url)
    quickload_files.append(quickload_file)
    description="alignments for LFY IP samples"
    fname=deploy_dir+"LFY.bam"
    track_name=igb_folder+"LFY IP, alignments"
    quickload_file=QuickloadFile(path=fname,
                                 foreground_color=foreground_color,
                                 tool_tip=description,
                                 track_name=track_name,
                                 track_info_url=track_info_url)
    quickload_files.append(quickload_file)
    description="coverage graph for LFY IP alignments"
    fname=deploy_dir+'LFY.bedgraph.gz'
    track_name=igb_folder+'/LFY IP, coverage'
    quickload_file=QuickloadFile(path=fname,
                                 foreground_color=foreground_color,
                                 tool_tip=description,
                                 track_name=track_name,
                                 track_info_url=track_info_url)
    quickload_files.append(quickload_file)

    description="alignments for control samples"
    fname=deploy_dir+"CNTRL.bam"
    track_name=igb_folder+"Control, alignments"
    foreground_color="006600"
    quickload_file=QuickloadFile(path=fname,
                                 foreground_color=foreground_color,
                                 tool_tip=description,
                                 track_name=track_name,
                                 track_info_url=track_info_url)
    quickload_files.append(quickload_file)
    description="coverage graph for control alignments"
    fname=deploy_dir+'CNTRL.bedgraph.gz'
    track_name=igb_folder+"Control, coverage"
    quickload_file=QuickloadFile(path=fname,
                                 foreground_color=foreground_color,
                                 tool_tip=description,
                                 track_name=track_name,
                                 track_info_url=track_info_url)

    quickload_files.append(quickload_file)
    return quickload_files

if __name__ == '__main__':
    about="Make annots.xml text for A_thaliana_Jun_2009 LFY ChIP-Seq files." 
    utils.checkForHelp(about)
    quickload_files=makeQuickloadFiles()
    txt = makeFileTagsXml(quickload_files)
    writeToFileStream(txt) 

