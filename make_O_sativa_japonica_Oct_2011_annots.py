#!/usr/bin/env python

from Quickload import *
from AnnotsXmlForRNASeq import *
import QuickloadUtils as utils

import os,sys

genome='O_sativa_japonica_Oct_2011'



w="FFFFFF"
# color for control, root
cr="1F78B4"
# color for control, shoot
cs="33A02C"
# color for treatment, root
tr='9900FF' 
# color for treatment, shoot
ts='8B2323'

rnaseq_deploy_dir='rnaseq/SRP049054'
u=genome + '/' + rnaseq_deploy_dir # track information URL; where users go if click "i" in Data Access Panel

samples =[['BA2h-R1','BA Root 1',tr,w,u],
         ['BA2h-R2','BA Root 2',tr,w,u],
         ['BA2h-R3','BA Root 3',tr,w,u],
         ['Control2h-R1','Mock Root 1',cr,w,u],
         ['Control2h-R2','Mock Root 2',cr,w,u],
         ['Control2h-R3','Mock Root 3',cr,w,u],
         ['BA2h-S1','BA Shoot 1',ts,w,u],
         ['BA2h-S2','BA Shoot 2',ts,w,u],
         ['BA2h-S3','BA Shoot 3',ts,w,u],
         ['Control2h-S1','Mock Shoot 1',cs,w,u],
         ['Control2h-S2','Mock Shoot 2',cs,w,u],
         ['Control2h-S3','Mock Shoot 3',cs,w,u]]


    
def getModels():
    quickload_files=[]

    # reference gene models, these should autoload
    title = "RGAP (MSU7)"
    descr="MSU7 gene models"
    fn=genome+'.bed.gz'
    foreground_color="000000"
    quickload_file = AnnotationFile(path=fn,tool_tip=descr,show2tracks=True,
                                    track_name=title,foreground_color=foreground_color,
                                    track_info_url=genome,direction_indicator="arrow",
                                    load_all=True)
    quickload_file.setTrackLabelFontSize(14)
    quickload_files.append(quickload_file)
    
    # RAP-DB gene models - don't autoload
    title="RAP-DB"
    descr="gene models from the Rice Annotation Project database"
    foreground_color="3300CC"
    fn="IRGSP-1.0_representative_2013-04-24.bed.gz" 
    quickload_file = AnnotationFile(path=fn,tool_tip=descr,track_info_url=genome,
                                    direction_indicator="arrow",show2tracks=True,
                                    track_name=title,foreground_color=foreground_color)
    quickload_file.setTrackLabelFontSize(14)
    quickload_files.append(quickload_file)

    return quickload_files

def makeQuickloadFiles():
    quickload_files = []
    for quickload_file in getModels():
        quickload_files.append(quickload_file)
    for quickload_file in makeQuickloadFilesForRNASeq(lsts=samples,
                                                      folder="RNA-Seq/BA treatment SRP049054",
                                                      deploy_dir=rnaseq_deploy_dir,
                                                      include_FJ=True):
        quickload_files.append(quickload_file)
    return quickload_files

if __name__ == '__main__':
    about="Make annots.xml text." 
    utils.checkForHelp(about)
    quickload_files=makeQuickloadFiles()
    main(quickload_files)
