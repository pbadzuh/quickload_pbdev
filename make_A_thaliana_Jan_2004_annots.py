#!/usr/bin/env python


import Quickload as q
import make_genome_annots as m
import QuickloadUtils as utils
import os,sys

genome = 'A_thaliana_Jan_2004'
s = """TAIR7_protein_coding_gene.bgn\tTAIR7 mRNA\tProtein coding gene models from TAIR7
TAIR7_mirna.bed.gz\tTAIR7 miRNA\tTAIR7 microRNA gene models
TAIR7_rrna.bed.gz\tTAIR7 rRNA\tTAIR7 rRNA gene models
TAIR7_pre-trna.bed.gz\tTAIR7 tRNA\tTAIR7 tRNA gene models
TAIR7_other_rna.bed.gz\tTAIR7 other RNA\tTAIR7 other RNA gene models
TAIR7_snorna.bed.gz\tTAIR7 snoRNA\tTAIR7 snoRNA gene models
TAIR7_pseudogene.bed.gz\tTAIR7 pseudogenes\tGene models that probably don't code for protein anymore
TAIR7_snrna.bed.gz\tTAIR7 snRNAs\tGene models encoding small nuclear RNAs"""
    
def makeQuickloadFiles():
    quickload_files = m.makeQuickloadFilesForGenomeRelease(files_string=s,
                                                reference_models_index=0,
                                                genome=genome)
    quickload_files[0].setLabelField('id')
    return quickload_files

if __name__ == '__main__':
    usage = """Make annots.xml for Arabidopsis assembly named TIGR5, TAIR6, and TAIR7."""
    utils.checkForHelp(usage) # prints help message if user supplies option -h or --help 
    quickload_files=makeQuickloadFiles()
    q.main(quickload_files)
