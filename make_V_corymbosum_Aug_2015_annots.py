#!/usr/bin/env python

from Quickload import * 
from make_V_corymbosum_scaffold_May_2013_annots import *
from AnnotsXmlForRNASeq import *
import QuickloadUtils as utils

genome='V_corymbosum_Aug_2015'
track_info_url='V_corymbosum_Aug_2015'
bb_url='V_corymbosum_Aug_2015/berrydev_proc'

bb_se=[['P1_ripe','ripe 2-41',ripe,w,bb_url],
       ['P2_ripe','ripe 2-42',ripe,w,bb_url],
       ['P3_ripe','ripe 3-33',ripe,w,bb_url],
       ['P1_pink','pink 2-41',pink,w,bb_url],
       ['P2_pink','pink 2-42',pink,w,bb_url],
       ['P3_pink','pink 3-33',pink,w,bb_url],
       ['P1_green','green 2-41',green,w,bb_url],
       ['P2_green','green 2-42',green,w,bb_url],
       ['P3_green','green 3-33',green,w,bb_url],
       ['P1_cup','cup 2-41',cup,w,bb_url],
       ['P2_cup','cup 2-42',cup,w,bb_url],
       ['P3_cup','cup 3-33',cup,w,bb_url],
       ['P1_pad','pad 2-41',pad,w,bb_url],
       ['P3_pad_a','pad 3-33a',pad,w,bb_url],
       ['P3_pad_b','pad 3-33b',pad,w,bb_url]]
  
def getBbGeneModels():
    quickload_files=[]
    fn="augustus-take6.bed.gz"
    title="Gene Predictions/Augustus"
    descr="Augustus gene models"
    foreground_color="000000"
    quickload_file = AnnotationFile(path=fn,tool_tip=descr,
                                    track_name=title,foreground_color=foreground_color,
                                    load_all=True,
                                    track_info_url=track_info_url)
    quickload_files.append(quickload_file)
    return quickload_files

def getBbOtherAnnots():
    quickload_files = []
    foreground_color='5959a6'
    track_label_font_size=16
    label_field="id"
    fn="repeats.bed.gz"
    title="Repeats"
    descr="Matches to repetitive elements"
    quickload_file = AnnotationFile(path=fn,tool_tip=descr,
                                    track_name=title,foreground_color=foreground_color,
                                    label_field=label_field,
                                    track_label_font_size=track_label_font_size,
                                    track_info_url=track_info_url)
    quickload_files.append(quickload_file)
    return quickload_files

def makeQuickloadFiles():
    quickload_files = []
    for quickload_file in getBbGeneModels():
        quickload_files.append(quickload_file)
    for quickload_file in getBbOtherAnnots():
        quickload_files.append(quickload_file)
    for quickload_file in makeQuickloadFilesForRNASeq(lsts=bb_se,deploy_dir=bb_url.split('/')[1],
                                folder=dev_course_folder):
        quickload_files.append(quickload_file)
    return quickload_files

if __name__ == '__main__':
    about="Make annots.xml text." 
    utils.checkForHelp(about)
    quickload_files=makeQuickloadFiles()
    main(quickload_files)
    
