#!/usr/bin/env python

from Quickload import *
from AnnotsXmlForRNASeq import *
import QuickloadUtils as utils

import os,sys

genome='M_acuminata_DH_Pahang_Jan_2016'

w="FFFFFF"
# control
cr="1F78B4"
# treatment
tr='9900FF' 

rnaseq_deploy_dir='rnaseq/SRP067207'
u=genome + '/' + rnaseq_deploy_dir # track information URL; where users go if click "i" in Data Access Panel

# see https://bitbucket.org/lorainelab/banana
samples = [
    ['SRR2984585','Cachaco C1',cr,w,u],
    ['SRR2984586','Cachaco C2',cr,w,u],
    ['SRR2984589','Cachaco C3',cr,w,u],
    ['SRR2984587','Cachaco T1',tr,w,u],
    ['SRR2984588','Cachaco T2',tr,w,u],
    ['SRR2984590','Cachaco T3',tr,w,u],
    ['SRR2984582','Grande Naine C1',cr,w,u],
    ['SRR2984591','Grande Naine C2',cr,w,u],
    ['SRR2984594','Grande Naine C3',cr,w,u],
    ['SRR2984592','Grande Naine T2',tr,w,u],
    ['SRR2984580','Grande Naine T1',tr,w,u],
    ['SRR2984595','Grande Naine T3',tr,w,u],
    ['SRR2984596','Mbwazirume C1',cr,w,u],
    ['SRR2984598','Mbwazirume C2',cr,w,u],
    ['SRR2984601','Mbwazirume C3',cr,w,u],
    ['SRR2984583','Mbwazirume T1',tr,w,u],
    ['SRR2984584','Mbwazirume T2',tr,w,u],
    ['SRR2984599','Mbwazirume T3',tr,w,u]]

    
def getModels():
    quickload_files=[]
    # reference gene models, these should autoload
    title = "Gene models"
    descr="Banana gene model hub gene models"
    fn=genome+'.bed.gz'
    foreground_color="000000"
    quickload_file = AnnotationFile(path=fn,
                                    tool_tip=descr,show2tracks=True,
                                    track_name=title,
                                    foreground_color=foreground_color,
                                    track_info_url=genome,
                                    direction_indicator="arrow",
                                    load_all=True)
    quickload_file.setTrackLabelFontSize(14)
    quickload_files.append(quickload_file)
    return quickload_files

def makeQuickloadFiles():
    quickload_files = []
    for quickload_file in getModels():
        quickload_files.append(quickload_file)
    for quickload_file in makeQuickloadFilesForRNASeq(lsts=samples,
                                                      folder="RNA-Seq/Drought SRP067207",
                                                      deploy_dir=rnaseq_deploy_dir,
                                                      include_FJ=True):
        quickload_files.append(quickload_file)
    return quickload_files

if __name__ == '__main__':
    about="Make annots.xml text." 
    utils.checkForHelp(about)
    quickload_files=makeQuickloadFiles()
    main(quickload_files)
